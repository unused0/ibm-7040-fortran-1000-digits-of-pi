# IBM 7040 FORTRAN 1000 digits of Pi

[](https://www.facebook.com/photo.php?fbid=10212163538957670&set=gm.2780093638698948&type=3&theater&ifg=1)

A FORTRAN program to compute 1000 digits of Pi on a IBM 7040.

Code copied from [](http://www.rosettacode.org/wiki/Pi#Fortran), ported to 7094 FORTRAN.